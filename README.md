Data
====


 * https://livelancsac-my.sharepoint.com/:f:/g/personal/rowlings_lancaster_ac_uk/EsJ0G_S8-_9IpK9-XcIdCDIBup6GBdn4wTt1jCHuCDWgQA?e=X4URKJ
 * https://drive.google.com/drive/folders/1voEJ167j6PoVRPRJknBS6Tq6M_vTOL7F?usp=sharing
 * https://lancaster.box.com/s/lzxyr6am49n0qyvpm5548o6mh2epd6qx


Slides
======

 * https://docs.google.com/presentation/d/1ACiWEM3_T8Kuzcdv8-wwXq_x2O38Vq9DMmtlezFLbFc/edit?usp=sharing

 * https://docs.google.com/presentation/d/1Q-EF-IqOdKFXv5Y4GjqwbRreMMtlojUiimJP-GFRJV8/edit?usp=sharing
 
 * https://docs.google.com/presentation/d/1WswuSvDX0mxI3vyzcul4wt8x5KTeivroVvM8Tkagxck/edit?usp=sharing
 
 * https://docs.google.com/presentation/d/13M0-hxXTUjLuv2G2MyLziuM_VjOaYjpm3gIxHJqJizA/edit?usp=sharing
